{if !$sd_back_to_topEmbedded}
    <a href="#" title="{__("up")}" class="sd-back-to-top {if $addons.sd_back_to_top.stripe_mode == 'left'}stripe stripe--left{else if $addons.sd_back_to_top.stripe_mode == 'right'}stripe stripe--right{else}stripe--off{/if} {if $addons.sd_back_to_top.elevator_pos == 'right'}elevator--right{else}elevator--left{/if}" id="sd_back_to_top">
        <div class="sd-back-to-top-icon">
            <i class="ty-icon-chevron-up"></i>
        </div>
    </a>
{/if}