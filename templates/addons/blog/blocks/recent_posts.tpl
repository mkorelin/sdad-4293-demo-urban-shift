{** block-description:blog.recent_posts **}

{if $items}
    <div class="ty-blog-sidebox">
        <ul class="ty-blog-sidebox__list">
            {foreach from=$items item="page"}
                <li class="ty-blog-sidebox__item">
                    <div class="ty-blog__recent_post_column">
                        <div class="ty-blog__date"><i class="ty-icon-blog-calendar"></i>{$subpage.timestamp|date_format:"`$settings.Appearance.date_format`"}</div>
                        <a href="{"pages.view?page_id=`$page.page_id`"|fn_url}">
                            <h2 class="ty-blog__post-title">
                                {$page.page}
                            </h2>
                        </a>
                    </div>
                </li>
            {/foreach}
        </ul>
    </div>
{/if}