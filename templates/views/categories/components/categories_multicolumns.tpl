{split data=$categories size=$columns|default:"3" assign="splitted_categories"}
<div class="ty-subcategories">
    {strip}
    {foreach from=$splitted_categories item="scats"}
        <div class="ty-subcategories-block">
            {foreach from=$scats item="category"}
                {if $category}
                    <div class="ty-column{$columns} ty-subcategories-block__item">
                        {if $category.main_pair}
                            <a href="{"categories.view?category_id=`$category.category_id`"|fn_url}" class="ty-subcategories-block__a">

                                    {include file="common/image.tpl"
                                        show_detailed_link=false
                                        images=$category.main_pair
                                        no_ids=true
                                        image_id="category_image"
                                        image_width=$settings.Thumbnails.category_lists_thumbnail_width
                                        image_height=$settings.Thumbnails.category_lists_thumbnail_height
                                        class="ty-subcategories-img"
                                    }

                                <div class="textblock">
                                    <h2>{$category.category}</h2>
                                    <span class="sdt-btn--large">{__("quick_view")}</span>
                                </div>
                            </a>
                        {else}
                            <a href="{"categories.view?category_id=`$category.category_id`"|fn_url}" class="ty-subcategories-block__a ty-subcategories-block-no-image">
                                <div class="textblock">
                                    <h2>{$category.category}</h2>
                                    <span class="sdt-btn--large">{__("quick_view")}</span>
                                </div>
                            </a>
                        {/if}
                    </div>
                {/if}
            {/foreach}
        </div>
    {/foreach}
    {/strip}
</div>

{capture name="mainbox_title"}{$title}{/capture}